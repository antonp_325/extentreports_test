package training;

import java.util.Iterator;

/**
 * Created by aptashnik on 12/5/2016.
 */
public class DataPortionIterator implements Iterator<Object[]> {

    private final String[][] data = {
            {"Hello,", " World!"},
            {"Szcxz", "dsvxc"}
    };
    private int index = 0;


    @Override
    public boolean hasNext() {
        return index < data.length;
    }

    @Override
    public Object[] next() {
        System.out.println("One data portion provided");
        return data[index++];
    }
}
