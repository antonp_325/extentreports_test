package training;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.testng.ITestContext;
import org.testng.annotations.*;
import org.testng.xml.XmlTest;
import training.utils.StepLog;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by aptashnik on 12/6/2016.
 */
//@Test
//public class BaseTest {
//    private static ExtentReports extentReport;
//    private static ThreadLocal<ExtentTest> testReportThreadLocal = new ThreadLocal<>(),
//            stepReportThreadLocal = new ThreadLocal<>();
//
//    @BeforeSuite
//    public void setUp() {
//        System.out.println("b suite");
//        extentReport = new ExtentReports();
//        ExtentHtmlReporter extentHtmlReporter = new ExtentHtmlReporter("target/report.html");
//        extentReport.attachReporter(extentHtmlReporter);
//        writeEnvInfo();
//    }
//
//
//    @AfterSuite
//    public void tearDown() {
//        //extentReport.flush();
//    }
//
//    @BeforeTest
//    public void beforeClass(ITestContext iTestContext) {
//        testReportThreadLocal.set(extentReport.createTest(iTestContext.getName()));
//    }
//
//
//    @AfterTest
//    public void afterTest() {
//        extentReport.flush();
//    }
//
//    @BeforeMethod
//    public void beforeStep(Method m) {
//        stepReportThreadLocal.set(testReportThreadLocal.get().createNode(m.getName()));
//        String[] groups = {};
//        org.testng.annotations.Test tAnno = m.getDeclaredAnnotation(org.testng.annotations.Test.class);
//        if(tAnno != null)
//            groups = tAnno.groups();
//        stepReportThreadLocal.get().assignCategory(groups);
//
//    }
//
//    private void writeEnvInfo() {
//        extentReport.setSystemInfo("OS", System.getProperty("os.name", "unknown"));
//        extentReport.setSystemInfo("OS version", System.getProperty("os.version", "unknown"));
//    }
//
//    protected ExtentTest stepReporter() {
//        return stepReportThreadLocal.get();
//    }
//}
@Test
public class BaseTest {
    private static final Map< String, Map<String, Map<String, List<StepLog>>>> logsContainer;

    static {
        logsContainer = new HashMap<>();
    }

    private static Map<String, Map<String, List<StepLog>>> currentSuiteLogs;

    private Map<String, List<StepLog>> currentTestLogs;
    private List<StepLog> currentStepLogs;


    public static Map<String, Map<String, Map<String, List<StepLog>>>> getLogContainer() {
        return logsContainer;
    }

    @BeforeSuite
    public void initLogsContainer() {
        currentSuiteLogs = new HashMap<>();
    }

    @AfterSuite
    public void saveSuiteLogs(XmlTest suite) {
        getLogContainer().put(suite.getSuite().getName(), currentSuiteLogs);
    }

    @BeforeTest
    public void initTestLogsContainer(ITestContext iTestContext) {
        currentTestLogs = new HashMap<>();
    }

    @AfterTest
    public void saveTestLogs(ITestContext iTestContext) {
        currentSuiteLogs.put(iTestContext.getName(), currentTestLogs);

    }

    @BeforeMethod
    public void initStepLogsContainer(Method m) {
        currentStepLogs = new ArrayList<>();
    }

    @AfterMethod
    public void saveStepLogs(Method m) {
        currentTestLogs.put(m.getName(), currentStepLogs);
        //System.out.println("Method: " + m.getName());
    }

    protected void log(Status status, String msg) {
        ExtentColor color;
        switch (status) {
            case FAIL:
            case FATAL:
            case ERROR:
                color = ExtentColor.RED;
                break;
            case INFO:
            case DEBUG:
                color = ExtentColor.BLUE;
                break;
            case SKIP:
                color = ExtentColor.YELLOW;
                break;
            case PASS:
                color = ExtentColor.GREEN;
                break;
            default:
                color = ExtentColor.WHITE;
        }
        currentStepLogs.add(new StepLog(status, MarkupHelper.createLabel(msg, color)));
    }

    protected void log(Status status, Throwable exception) {
        throw new UnsupportedOperationException();
    }

    protected void log(String msg) {
        log(Status.INFO, msg);
    }

    protected void fatal(String msg) {
        log(Status.FATAL, msg);
    }
}