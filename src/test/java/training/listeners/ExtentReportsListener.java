package training.listeners;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import org.testng.*;
import org.testng.xml.XmlSuite;
import training.BaseTest;
import training.utils.StepLog;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by aptashnik on 12/7/2016.
 */
public class ExtentReportsListener implements IReporter {
    private static final String REPORTS_DIR_NAME = "ExtentReports";
    private ExtentReports extent;
    private ExtentTest testReport;
    private Map<String, List<StepLog>> testLogs;

    @Override
    public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
        outputDirectory = outputDirectory + File.separator + REPORTS_DIR_NAME;
        try {
            Files.createDirectories(Paths.get(outputDirectory));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Map<String, Map<String, Map<String, List<StepLog>>>> logs = BaseTest.getLogContainer();

        assert logs != null;

        IResultMap passed, failed, skipped;

        for (ISuite suite : suites) {
            System.out.println("Suite name: " + suite.getName());
            extent = new ExtentReports();
            extent.attachReporter(
                    new ExtentHtmlReporter(outputDirectory + File.separator + suite.getName() + ".html"));
            writeEnvInfo(extent);
            Map<String, ISuiteResult> result = suite.getResults();

            for (ISuiteResult r : result.values()) {
                ITestContext context = r.getTestContext();
                testReport = extent.createTest(context.getName());
                testLogs = logs.get(suite.getName()).get(context.getName());

                passed = context.getPassedTests();
                failed = context.getFailedTests();
                skipped = context.getSkippedTests();

                ExtentTest test;
                List<StepLog> stepLogs;
                for (ITestNGMethod testNGMethod : context.getAllTestMethods()) {
                    test = testReport.createNode(testNGMethod.getMethodName());
                    if (testLogs != null) {
                        stepLogs = testLogs.get(testNGMethod.getMethodName());

                        if (stepLogs != null)
                            for (StepLog log : stepLogs)
                                test.log(log.getStatus(), log.getDescription());
                    }
                    test.assignCategory(testNGMethod.getGroups());

                    Set<ITestResult> iTestResults;
                    if (!(iTestResults = passed.getResults(testNGMethod)).isEmpty())
                        for (ITestResult v : iTestResults) {
                            if (v.getThrowable() != null)
                                test.fatal(v.getThrowable());
                            else {
                                String message = "Test " + Status.PASS.toString().toLowerCase() + "ed";
                                test.log(Status.PASS, message);
                            }
                        }
                    else if (!(iTestResults = failed.getResults(testNGMethod)).isEmpty()) {
                        for (ITestResult v : iTestResults) {
                            if (v.getThrowable() != null)
                                test.fatal(v.getThrowable());
                            else {
                                String message = "Test " + Status.FAIL.toString().toLowerCase() + "ed";
                                test.log(Status.FAIL, message);
                            }
                        }
                    } else if (!(iTestResults = skipped.getResults(testNGMethod)).isEmpty()) {
                        for (ITestResult v : iTestResults) {
                            if (v.getThrowable() != null)
                                test.fatal(v.getThrowable());
                            else {
                                String message = "Test " + Status.SKIP.toString().toLowerCase() + "ped";
                                test.log(Status.SKIP, message);
                            }
                        }
                    }
                }
//                buildTestNodes(context.getPassedTests(), Status.PASS);
//                buildTestNodes(context.getFailedTests(), Status.FAIL);
//                buildTestNodes(context.getSkippedTests(), Status.SKIP);
            }
            extent.flush();
        }
    }



//    private void buildTestNodes(IResultMap tests, Status status) {
//        ExtentTest test;
//        List<StepLog> stepLogs;
//        for (ITestResult result : tests.getAllResults()) {
//            test = testReport.createNode(result.getMethod().getMethodName());
//            if (testLogs != null) {
//                stepLogs = testLogs.get(result.getMethod().getMethodName());
//
//                //assert stepLogs != null;
//
//                if (stepLogs != null)
//                    for (StepLog log : stepLogs)
//                        test.log(log.getStatus(), log.getDescription());
//            }
//            test.assignCategory(result.getMethod().getGroups());
//            if (result.getThrowable() != null)
//                test.fatal(result.getThrowable());
//            else {
//                String message = "Test " + status.toString().toLowerCase() + "ed";
//                test.log(status, message);
//            }
//        }
//    }

    private void writeEnvInfo(ExtentReports extentReports) {
        extentReports.setSystemInfo("OS", System.getProperty("os.name", "unknown"));
        extentReports.setSystemInfo("OS version", System.getProperty("os.version", "unknown"));
    }
}
