package training;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by aptashnik on 12/6/2016.
 */
public class TestCase3 extends BaseTest {
    public void toDoSomethingUseful() {
        Gson gsonService = new GsonBuilder().create();

        try(FileReader file = new FileReader("jsonFile.json")) {
            //file.write(gsonService.toJson(new Abc("three", 3)));
            Abc obj = gsonService.fromJson(file, Abc.class);
            assert obj.n == 3;
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}