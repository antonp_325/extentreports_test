package training.utils;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.Markup;

/**
 * Created by aptashnik on 12/7/2016.
 */
public class StepLog {
    private Status status;
    private Markup description;

    public Status getStatus() {
        return status;
    }

    public Markup getDescription() {
        return description;
    }

    public StepLog(Status status, Markup description) {
        this.status = status;
        this.description = description;
    }
}
