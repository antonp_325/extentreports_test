package training;

import org.testng.*;

import java.util.List;

/**
 * Created by aptashnik on 05.12.2016.
 */
public class SimpleSuiteListener extends TestListenerAdapter implements IMethodInterceptor {
    public List<IMethodInstance> intercept(List<IMethodInstance> list, ITestContext iTestContext) {
        System.out.println("Interceptor");
        for(IMethodInstance mInst : list)
            System.out.println(mInst.getMethod().getMethodName());
        return list;
    }

    @Override
    public void onStart(ITestContext testContext) {
        super.onStart(testContext);
        System.out.println(testContext.getName() + " started");

    }
}
//extends TestListenerAdapter