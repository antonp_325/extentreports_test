package training;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import org.testng.annotations.*;
import org.testng.annotations.Test;

/**
 * Created by aptashnik on 12/6/2016.
 */
public class TestCase2 extends BaseTest {

    public void typeLogin() {
        log("ok");
    }
    @Test(dependsOnMethods = "typeLogin")
    public void getTempPassFromEmail() {
        log("ok");
    }
    @Test(dependsOnMethods = "getTempPassFromEmail")
    public void tryToLogin() {
        log("ok");
    }
}
