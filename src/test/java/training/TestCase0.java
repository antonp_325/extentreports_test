package training;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.Status;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

/**
 * Created by aptashnik on 12/7/2016.
 */
@Test
public class TestCase0 extends BaseTest {
    @Test(groups = "hightPrior")
    public void t() {

        Assert.assertTrue(false, "some explanation of failure");
    }
    public void t2() {
        log(Status.INFO, "hello");
    }
    public void exc() {
        log("hello");
        throw new IllegalStateException();
    }
}
