package training;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.lang.reflect.Method;

/**
 * Created by aptashnik on 05.12.2016.
 */
@Test
public class TestCase1 extends BaseTest {


//    @DataProvider
//    private Iterator<Object[]> dataProvider() {
//        return new training.DataPortionIterator();
//    }

//    public void indep1(String a1, String a2) {
//        System.out.println(a1 + " - " + a2);
//    }


    @Test(testName = "first method", groups = {"g1", "gAccept"})
    public void goToSite() {
//        stepReporter().log(Status.INFO, "infooooooo");
//
//        try {
//
//            stepReporter().fatal("screeeeeeen",
//                    MediaEntityBuilder.createScreenCaptureFromPath("C:/Users/aptashnik/Documents/testing_services.png", "fscreen")
//                            .build());
//            stepReporter().addScreenCaptureFromPath("C:/Users/aptashnik/Documents/testing_services.png");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        stepReporter().log(Status.PASS, "passssss");

        //extentTest.pass("test done");
        //Assert.assertTrue(false);
        log("goToSite - msg1");
        log("goToSite - msg2");
        log(Status.SKIP, "goToSite - msg3");
        //Assert.assertTrue(false, "error msg");
    }

    @Test(dependsOnMethods = "goToSite", groups = "g1")
    public void signin() {
//        stepReporter().info("step1");
//        stepReporter().info(MarkupHelper.createLabel("hex", ExtentColor.YELLOW));
//        stepReporter().info(MarkupHelper.createCodeBlock("syncronized s()"));
//        try {
//            stepReporter().pass("ok").addScreenCaptureFromPath("testing_services.png", "screen");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        //extentTest.fail("errrrrrror");
        //Assert.assertTrue(false);
        log("signin - msg1");
        log("signin - msg2");

    }

    public void tExcept() {
        throw new UnsupportedOperationException();
    }

    @Test(dependsOnMethods = "signin")
    public void doSomeAction() {
        log("doSomeAction - msg1");
        log("doSomeAction - msg2");
        fatal("doSomeAction - errrrrors");
    }


}